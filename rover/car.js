import { Position, TrackPosition } from "./position";
import { Helicopter } from "./helicopter";
import { Direction, Order, TrackType, xArr, yArr } from "./constant";

/**
 * @Desc   火星车类
 * @Params
 * @Return
*/
export class Car {
  constructor(position) {
    this.position = position;
    this.direction = Direction.north;
    this.helicopter = new Helicopter(position);
    this.trackPositions = position ? [position.copy()] : [];
  }

  land() {
    if (this.position) { return; }

    let maxX = xArr.length - 1;
    let maxY = yArr.length - 1;
    let randomX = Math.floor(Math.random() * maxX) + 1;
    let randomY = Math.floor(Math.random() * maxY) + 1;
    this.position = new Position(randomX, randomY);
    this.helicopter.position = this.position;
    this.trackPositions = [this.position.copy()];
  }

  turnLeft() {
    switch (this.direction) {
      case Direction.north:
        this.direction = Direction.west;
        break;
      case Direction.south:
        this.direction = Direction.east;
        break;
      case Direction.west:
        this.direction = Direction.south;
        break;
      case Direction.east:
        this.direction = Direction.north;
        break;
      default:
        break;
    }
  }

  turnRight() {
    switch (this.direction) {
      case Direction.north:
        this.direction = Direction.east;
        break;
      case Direction.south:
        this.direction = Direction.west;
        break;
      case Direction.west:
        this.direction = Direction.north;
        break;
      case Direction.east:
        this.direction = Direction.south;
        break;
      default:
        break;
    }
  }

  move(step: number = 1) {
    if (isNaN(step)) { return; }
    if (!this.position) { return; }

    let {x, y} = this.position;
    let new_x = x;
    let new_y = y;

    switch (this.direction) {
      case Direction.north:
        new_y = y - step;
        break;
      case Direction.east:
        new_x = x + step;
        break;
      case Direction.south:
        new_y = y + step;
        break;
      case Direction.west:
        new_x = x - step;
        break;
      default:
        break;
    }

    let minX = 1;
    let maxX = xArr.length - 1;
    let minY = 1;
    let maxY = yArr.length - 1;

    if (new_x > maxX) {
      this.position.x = maxX;
    } else if (new_x < minX) {
      this.position.x = minX;
    } else {
      this.position.x = new_x;
    }

    if (new_y > maxY) {
      this.position.y = maxY;
    } else if (new_y < minY) {
      this.position.y = minY;
    } else {
      this.position.y = new_y;
    }

    let position = this.position.copy();

    if (this.trackPositions.find((p) => p.x === position.x && p.y === position.y) === undefined) {
      this.trackPositions.push(position);
    }
  }

  takeOffPlane() {
    this.helicopter.fly()
  }

  executeOrder(order: string) {
    if (!order) { return }
    let upperOrder = order.toUpperCase();
    for (let i = 0; i < upperOrder.length; i++) {
      let char = upperOrder.charAt(i);
      switch (char) {
        case Order.goForward:   // 进 1
          this.move(1);
          break;
        case Order.goBack:      // 退 1
          this.move(-1);
          break;
        case Order.turnLeft:    // 左转
          this.turnLeft();
          break;
        case Order.turnRight:   // 右转
          this.turnRight();
          break;
        case Order.takeOffPlane: // 停止, 放飞机
          this.takeOffPlane();
          break;
      }
    }
  }

  mergeTrackPositions() {
    let trackPositions = this.trackPositions;
    let helicopterTrackPositions = this.helicopter.trackPositions;
    let sumTrackPositions = [];

    trackPositions.forEach((position) => {
      if (helicopterTrackPositions.find((p) => p.x === position.x && p.y === position.y) === undefined) {
        sumTrackPositions.push(new TrackPosition(position.x, position.y, TrackType.car));
      } else {
        sumTrackPositions.push(new TrackPosition(position.x, position.y, TrackType.both));
      }
    });

    helicopterTrackPositions.forEach((position) => {
      if (sumTrackPositions.find((p) => p.x === position.x && p.y === position.y) === undefined) {
        sumTrackPositions.push(new TrackPosition(position.x, position.y, TrackType.helicopter));
      }
    });

    return sumTrackPositions
  }
}
