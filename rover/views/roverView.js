import React, { useState } from "react";
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  useWindowDimensions
} from "react-native";
import { Position, TrackPosition } from "../position";
import { xArr, yArr, TrackType, Order } from "../constant";
import { Car } from "../car";
import { RoverMap } from "./roverMap";
import { LandResultView } from "./landResultView";
import { OrderView } from "./orderView";


const RoverView = (props) => {
  const {percy} = props;
  const [trackOrder, setTrackOrder] = useState('');

  const executeOrder = (order: string) => {
    if (!order) { return }
    percy?.executeOrder(order);
    let newOrder = trackOrder.slice();
    setTrackOrder(newOrder += `${order};`);
  }

  const trackPositions = percy?.mergeTrackPositions();

  return (
    <View style={{flex: 1}}>
      <RoverMap
        trackPositions={trackPositions}
      />
      <LandResultView
        trackOrder={trackOrder}
        curPosition={percy?.position}
        trackArea={`${trackPositions.length * 100 / ((xArr.length - 1) * (yArr.length - 1))}%`}
      />
      <OrderView
        onSend={(order) => {
          console.log(order);
          executeOrder(order);
        }}
      />
    </View>
  )
};

export default RoverView
