import { Text, View } from "react-native";
import { xArr } from "../constant";
import React from "react";

/**
 * @Desc   登陆结果显示
 * @Params
 * @Return
*/
export const LandResultView = (props) => {
  const {trackOrder, curPosition, trackArea} = props;
  return (
    <View style={{padding: 30}}>
      <Text style={{fontSize: 18}} numberOfLines={2}>{trackOrder ? `接收指令：${trackOrder}` : `接收指令：`}</Text>
      <Text style={{fontSize: 18, marginTop: 20}}>{curPosition ? `最终坐标：${xArr[curPosition.x]}${curPosition.y}` : `最终坐标：`}</Text>
      <Text style={{fontSize: 18, marginTop: 20}}>{trackArea ? `巡视范围：${trackArea}` : `巡视范围：`}</Text>
    </View>
  )
}
