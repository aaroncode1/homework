import { Text, useWindowDimensions, View } from "react-native";
import React from "react";
import { mapPositions, TrackType, xArr } from "../constant";

/**
 * @Desc   地图显示
 * @Params
 * @Return
 */
export const RoverMap = (props) => {
  const {trackPositions} = props;
  const {width, height} = useWindowDimensions();
  const length = Math.min(width, height);
  return (
    <View style={{width: length, height: length, backgroundColor: 'rgba(165, 202, 141, 1)', borderTopWidth: 1, borderTopColor: 'gray'}}>
      {
        mapPositions().map((arr, index) => {
          return (
            <View key={`row${index}`} style={{flex: 1, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray'}}>
              {arr.map((position) => {
                let trackPosition = (trackPositions || []).find((p) => p.x === position.x && p.y === position.y);
                return <PositionItem key={`${position.x}${position.y}`} position={position} trackPosition={trackPosition} />
              })}
            </View>
          )
        })
      }
    </View>
  )
}

const PositionItem = (props) => {
  let {position, trackPosition} = props;
  let {x, y} = position;
  let title = '';
  let bgColor = 'white';
  if (x === 0 && y === 0) {
    title = '';
  } else if (x === 0) {
    title = `${y}`;
  } else if (y === 0) {
    title = `${xArr[x]}`;
  } else {
    title = `${xArr[x]}${y}`;
    if (trackPosition) {
      let {type} = trackPosition;
      if (type === TrackType.helicopter) {
        bgColor = 'rgba(157, 195, 227, 1)';
      } else {
        bgColor = 'rgba(254, 216, 111, 1)';
      }
    } else {
      bgColor = 'rgba(0, 0, 0, 0)';
    }
  }
  return (
    <View style={{flex: 1, borderRightWidth: 1, borderRightColor: 'gray', justifyContent: 'center', alignItems: 'center', backgroundColor: bgColor}}>
      <Text style={{fontSize: 5}}>{title}</Text>
    </View>
  )
}
