import React, { useState } from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";
import { opOrders, Order } from "../constant";

/**
 * @Desc   指令输入
 * @Params
 * @Return
*/
export const OrderView = (props) => {
  const {onSend} = props;
  const [value, setValue] = useState('');
  return (
    <View style={{flex: 1, paddingHorizontal: 20}}>
      <OrderInput
        value={value}
        onSend={(order) => {
          onSend && onSend(order);
          setValue('');
        }}
      />
      <OrderPad
        onInput={(text) => {
          let newValue = value.slice();
          newValue += text;
          setValue(newValue);
        }}
        onClear={() => {
          setValue('')
        }}
      />
    </View>
  )
}

const OrderInput = (props) => {
  const {value, onSend} = props;
  return (
    <View style={{flexDirection: 'row', height: 50}}>
      <View style={{flex: 1, paddingHorizontal: 6, borderRadius: 6, backgroundColor: 'white'}}>
        <TextInput
          style={{flex: 1, fontSize: 18}}
          editable={false}
          value={value || '请输入指令...'}
        />
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={{justifyContent: 'center', alignItems: 'center', width: 80, borderRadius: 6, marginLeft: 10, backgroundColor: '#365496'}}
        onPress={() => {
          onSend && onSend(value)
        }}
      >
        <Text style={{fontSize: 18, color: 'white'}}>{'Send'}</Text>
      </TouchableOpacity>
    </View>
  )
}

const OrderPad = (props) => {
  const PadItem = (title, marginHorizontal, onInput, onClear) => {
    return (
      <View key={`${title}`} style={{flex: 1, marginHorizontal: marginHorizontal || 0}}>
        <TouchableOpacity
          activeOpacity={0.8}
          style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderRadius: 8}}
          onPress={() => {
            if (title === Order.clear) {
              onClear && onClear()
            } else {
              onInput && onInput(title)
            }
          }}
        >
          <Text style={{fontSize: 20}}>{title}</Text>
        </TouchableOpacity>
      </View>
    )
  }
  const {onInput, onClear} = props;
  const orders = opOrders();
  return (
    <View style={{flex: 1, paddingVertical: 20}}>
      <View style={{flex: 1, flexDirection: 'row'}}>
        {
          orders.slice(0, 3).map((order, index) => PadItem(order, index === 1 ? 20 : 0, onInput, onClear))
        }
      </View>
      <View style={{flex: 1, flexDirection: 'row', marginTop: 20}}>
        {
          orders.slice(3).map((order, index) => PadItem(order, index === 1 ? 20 : 0, onInput, onClear))
        }
      </View>
    </View>
  )
}
