import { Position } from "./position";
import { xArr, yArr } from "./constant";

/**
 * @Desc   直升机类
 * @Params
 * @Return
*/
export class Helicopter {
  constructor(position) {
    this.position = position;
    this.trackPositions = position ? [position.copy()] : [];
  }

  fly() {
    if (!this.position) { return; }
    let {x, y} = this.position;

    let minX = Math.max(x-1, 1);
    let maxX = Math.min(x+1, xArr.length - 1);
    let minY = Math.max(y-1, 1);
    let maxY = Math.min(y+1, yArr.length - 1);

    for (let i = minX; i <= maxX; i++) {
      for (let j = minY; j <= maxY ; j++) {
        if (x !== i || y !== j) {
          this.trackPositions.push(new Position(i, j))
        }
      }
    }
  }
}
