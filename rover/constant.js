import { Position } from "./position";

/**
 * 绘制地图所需坐标点数组
*/
const xArr = ['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y'];
const yArr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];

const mapPositions = () => {
  return yArr.map((y) => yArr.map((x) => new Position(x, y)));
};

const Direction = {
  north: 0,    // north
  east: 1,     // east
  south: 2,    // south
  west: 3      // west
};

const TrackType = {
  car: 0,
  helicopter: 1,
  both: 2
};

const Order = {
  goForward: 'F',
  goBack: 'B',
  turnLeft: 'L',
  turnRight: 'R',
  takeOffPlane: 'H',
  clear: 'CLR'
};

const opOrders = () => {
  return [Order.goForward, Order.goBack, Order.turnLeft, Order.turnRight, Order.takeOffPlane, Order.clear];
};

export {
  xArr,
  yArr,
  mapPositions,
  Direction,
  TrackType,
  Order,
  opOrders
}
