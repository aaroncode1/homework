/**
 * @Desc   坐标类
 * @Params
 * @Return
*/
export class Position {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  copy() {
    return Object.assign({}, this)
  }
}

/**
 * @Desc   运行轨迹类
 * @Params
 * @Return
*/
export class TrackPosition {
  constructor(x, y, type) {
    this.x = x;
    this.y = y;
    this.type = type;
  }
}
