# Build and run the app

This guide describes how to build and run the app so you can develop it.

## Main steps

Before starting, install these dependencies if you don't have them:
* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/en/download/package-manager/): use the
  **latest 12.x** version, not 14.x or later
* [Yarn](https://yarnpkg.com/en/docs/install), latest stable version

Then, run the commands below in your terminal:
```
git clone https://gitlab.com/aaroncode1/homework.git
cd homework
yarn install
```

If you're starting with iOS development, be sure to [install
CocoaPods](https://guides.cocoapods.org/using/getting-started.html)

Then, run the commands below in your terminal:
```
cd ios
pod install
```

Continue those instructions until you can run the homework app
with one of the following:

- `react-native run-ios`
- `react-native run-android`
- in Xcode
- in Android Studio

You'll want to be able to use both an emulator and a physical device; but
for starting out, just get either one working so you can play with the app.

## Screen Shot
![](./screenShot.png)

