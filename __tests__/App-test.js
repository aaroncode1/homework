/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { Car } from "../rover/car";
import { Position } from "../rover/position";
import { Direction } from "../rover/constant";

it('renders correctly', () => {
  renderer.create(<App />);
});

test('The landing position should be in the map.', () => {
  let percy = new Car();
  percy.land();
  expect(percy.position.x).toBeGreaterThanOrEqual(1);
  expect(percy.position.x).toBeLessThanOrEqual(25);
  expect(percy.position.y).toBeGreaterThanOrEqual(1);
  expect(percy.position.y).toBeLessThanOrEqual(25);
})

test('The direction should be west after turning left with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.turnLeft();
  expect(percy.direction).toBe(Direction.west);
})

test('The direction should be east after turning right with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.turnRight();
  expect(percy.direction).toBe(Direction.east);
})

test('The final position should not be changed after moving forward 1 step with initial position A1(1, 1).', () => {
  let percy = new Car(new Position(1, 1));
  percy.move(1);
  expect(percy.position.x).toBe(1);
  expect(percy.position.y).toBe(1);
})

test('The final position should not be changed after moving back 1 step with initial position A25(1, 25).', () => {
  let percy = new Car(new Position(1, 25));
  percy.move(-1);
  expect(percy.position.x).toBe(1);
  expect(percy.position.y).toBe(25);
})

test('The final position should not be changed after executing order: "BRFRF" with initial position Y25(25, 25).', () => {
  let percy = new Car(new Position(25, 25));
  percy.executeOrder("BRFRF");
  expect(percy.position.x).toBe(25);
  expect(percy.position.y).toBe(25);
})

test('The length of track positions should be 4(0.64%) after taking off the helicopter with initial position A1(1, 1).', () => {
  let percy = new Car(new Position(1, 1));
  percy.takeOffPlane();
  expect(percy.trackPositions.length).toBe(1);
})

test('The length of helicopter track positions should be 4(0.64%) after taking off the helicopter at initial position A1(1, 1).', () => {
  let percy = new Car(new Position(1, 1));
  percy.helicopter.fly();
  expect(percy.helicopter.trackPositions.length).toBe(4);
})

test('The final position should be F11(6, 11) after executing order: "FLFLFLFL" with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.executeOrder('FLFLFLFL');
  expect(percy.position.x).toBe(8);
  expect(percy.position.y).toBe(17);
  expect(percy.direction).toBe(Direction.north);
})

test('The final position should be F11(6, 11) after executing order: "FRFRFRFR" with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.executeOrder('FRFRFRFR');
  expect(percy.position.x).toBe(8);
  expect(percy.position.y).toBe(17);
  expect(percy.direction).toBe(Direction.north);
})

test('The final position should be F11(6, 11) after executing order: "FFFLFFRBBHFFFFF" with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.executeOrder('FFFLFFRBBHFFFFF');
  expect(percy.position.x).toBe(6);
  expect(percy.position.y).toBe(11);
})

test('The length of track positions should be 11(1.76%) after executing order: "FFFLFFRBBHFFFFF" with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.executeOrder('FFFLFFRBBHFFFFF');
  expect(percy.trackPositions.length).toBe(11);
})

test('The length of merge track positions should be 18(2.88%) after executing order: "FFFLFFRBBHFFFFF" with initial position H17(8, 17).', () => {
  let percy = new Car(new Position(8, 17));
  percy.executeOrder('FFFLFFRBBHFFFFF');
  expect(percy.mergeTrackPositions().length).toBe(18);
})
